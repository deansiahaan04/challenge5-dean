package com.example.challenge5.game

import android.content.Intent
import android.os.Bundle
import android.util.Log
import android.view.LayoutInflater
import android.widget.Button
import android.widget.ImageView
import android.widget.TextView
import android.widget.Toast
import androidx.appcompat.app.AlertDialog
import androidx.appcompat.app.AppCompatActivity
import androidx.core.content.ContextCompat
import com.example.challenge5.R
import com.example.challenge5.pilihangame.MainActivityPilihan

class MainActivityPemain : AppCompatActivity(), Callback {

    private lateinit var kembali: ImageView
    private lateinit var btnPemain1: ImageView
    private lateinit var btnPemain12: ImageView
    private lateinit var btnPemain13: ImageView
    private lateinit var btnCom1: ImageView
    private lateinit var btnCom12: ImageView
    private lateinit var btnCom13: ImageView
    private lateinit var btnRefresh: ImageView
    private lateinit var imageStatus: ImageView
    var pilihanPemain = ""
    private lateinit var txtHasil: String

    private val controller = Controller(this)

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main_pemain)

        val nama = intent.getStringExtra("Name")
        val textView = findViewById<TextView>(R.id.pemain1)
        textView.setText("$nama").toString()

        kembali = findViewById(R.id.kembali)

        kembali.setOnClickListener {
            val intent = Intent(this, MainActivityPilihan::class.java)
            intent.putExtra("Name", nama.toString())
            startActivity(intent)
        }

        btnPemain1 = findViewById(R.id.batu1)
        btnPemain12 = findViewById(R.id.kertas1)
        btnPemain13 = findViewById(R.id.gunting1)
        imageStatus = findViewById(R.id.vs)
        btnRefresh = findViewById(R.id.refresh)


        var clicked = true

        btnPemain1.setOnClickListener {
            if (clicked) {
                pilihanPemain = "Batu"
                btnPemain1.setBackgroundColor(ContextCompat.getColor(this, R.color.bg1))
                clicked = true
                btnPemain12.isEnabled = false
                btnPemain13.isEnabled = false
                Toast.makeText(this, "$nama Memilih Batu", Toast.LENGTH_SHORT).show()

            }
        }

        btnPemain12.setOnClickListener {
            if (clicked) {
                pilihanPemain = "Kertas"
                btnPemain12.setBackgroundColor(ContextCompat.getColor(this, R.color.bg1))
                clicked = true
                btnPemain1.isEnabled = false
                btnPemain13.isEnabled = false
                Toast.makeText(this, "$nama Memilih Kertas", Toast.LENGTH_SHORT).show()
            }
        }

        btnPemain13.setOnClickListener {
            if (clicked) {
                pilihanPemain = "Gunting"
                btnPemain13.setBackgroundColor(ContextCompat.getColor(this, R.color.bg1))
                clicked = true
                btnPemain1.isEnabled = false
                btnPemain12.isEnabled = false
                Toast.makeText(this, "$nama Memilih Gunting", Toast.LENGTH_SHORT).show()

            }
        }

        btnCom1 = findViewById(R.id.batu21)
        btnCom12 = findViewById(R.id.kertas21)
        btnCom13 = findViewById(R.id.gunting21)

        btnCom1.setOnClickListener {
            if (clicked) {
                val plhpemain = pilihanPemain
                controller.hitung(plhpemain, "Batu", nama)
                btnCom1.setBackgroundColor(ContextCompat.getColor(this, R.color.bg1))
                clicked = false
                Toast.makeText(this, "Pemain 2 Memilih Batu", Toast.LENGTH_SHORT).show()
                val view = LayoutInflater.from(this).inflate(R.layout.dialog1, null, false)
                val alert = AlertDialog.Builder(this)
                alert.setView(view)
                alert.setCancelable(false)

                val dialog = alert.create()
                dialog.show()

                val textView4 = dialog.findViewById<TextView>(R.id.hasilpermainan)
                textView4?.setText(txtHasil).toString()

                val btnMain = view.findViewById<Button>(R.id.buttonmain)
                btnMain.setOnClickListener {
                    val intent = Intent(this, MainActivityPemain::class.java)
                    intent.putExtra("Name", nama.toString())
                    startActivity(intent)
                }

                val btnKembali = view.findViewById<Button>(R.id.buttonkembali)
                btnKembali.setOnClickListener {
                    val intent = Intent(this, MainActivityPilihan::class.java)
                    intent.putExtra("Name", nama.toString())
                    startActivity(intent)
                }
            }
        }

        btnCom12.setOnClickListener {
            if (clicked) {
                val plhpemain = pilihanPemain
                controller.hitung(plhpemain, "Kertas", nama)
                btnCom12.setBackgroundColor(ContextCompat.getColor(this, R.color.bg1))
                clicked = false
                Toast.makeText(this, "Pemain 2 Memilih Kertas", Toast.LENGTH_SHORT).show()
                val view = LayoutInflater.from(this).inflate(R.layout.dialog1, null, false)
                val alert = AlertDialog.Builder(this)
                alert.setView(view)
                alert.setCancelable(false)

                val dialog = alert.create()
                dialog.show()

                val textView4 = dialog.findViewById<TextView>(R.id.hasilpermainan)
                textView4?.setText(txtHasil).toString()


                val btnMain = view.findViewById<Button>(R.id.buttonmain)
                btnMain.setOnClickListener {
                    val intent = Intent(this, MainActivityPemain::class.java)
                    intent.putExtra("Name", nama.toString())
                    startActivity(intent)
                }

                val btnKembali = view.findViewById<Button>(R.id.buttonkembali)
                btnKembali.setOnClickListener {
                    val intent = Intent(this, MainActivityPilihan::class.java)
                    intent.putExtra("Name", nama.toString())
                    startActivity(intent)
                }
            }
        }

        btnCom13.setOnClickListener {
            if (clicked) {
                val plhpemain = pilihanPemain
                btnCom13.setBackgroundColor(ContextCompat.getColor(this, R.color.bg1))
                clicked = false
                controller.hitung(plhpemain, "Gunting", nama)
                Toast.makeText(this, "Pemain 2 Memilih Gunting", Toast.LENGTH_SHORT).show()
                val view = LayoutInflater.from(this).inflate(R.layout.dialog1, null, false)
                val alert = AlertDialog.Builder(this)
                alert.setView(view)
                alert.setCancelable(false)

                val dialog = alert.create()
                dialog.show()

                val textView4 = dialog.findViewById<TextView>(R.id.hasilpermainan)
                textView4?.setText(txtHasil).toString()


                val btnMain = view.findViewById<Button>(R.id.buttonmain)
                btnMain.setOnClickListener {
                    val intent = Intent(this, MainActivityPemain::class.java)
                    intent.putExtra("Name", nama.toString())
                    startActivity(intent)
                }

                val btnKembali = view.findViewById<Button>(R.id.buttonkembali)
                btnKembali.setOnClickListener {
                    val intent = Intent(this, MainActivityPilihan::class.java)
                    intent.putExtra("Name", nama.toString())
                    startActivity(intent)
                }
            }
        }

        btnRefresh.setOnClickListener()
        {
            btnPemain1.setBackgroundColor(ContextCompat.getColor(this, R.color.bg))
            btnPemain12.setBackgroundColor(ContextCompat.getColor(this, R.color.bg))
            btnPemain13.setBackgroundColor(ContextCompat.getColor(this, R.color.bg))
            btnCom1.setBackgroundColor(ContextCompat.getColor(this, R.color.bg))
            btnCom12.setBackgroundColor(ContextCompat.getColor(this, R.color.bg))
            btnCom13.setBackgroundColor(ContextCompat.getColor(this, R.color.bg))
            imageStatus.setImageResource(R.drawable.vs)
            clicked = true

        }

    }


    override fun kirimStatus(status: String) {
        txtHasil = status
        when {
            status.contains("1") -> {
                imageStatus.setImageResource(R.drawable.vs)
                Log.i("Hasil", "Pemain1 Menang")
            }
            status.contains("2") -> {
                imageStatus.setImageResource(R.drawable.vs)
                Log.i("Hasil", "Pemain2 Menang")
            }
            else -> {
                imageStatus.setImageResource(R.drawable.vs)
                Log.i("Hasil", "Draw")


            }
        }


    }

    override fun kirimPemain(pemain2: String) {

    }
}




